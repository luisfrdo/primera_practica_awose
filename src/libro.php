<?php
class Libro {
    private $id_libro;
    private $titulo = "";
    private $autor = "";
    private $ano_pub;
    private $paginas;
    private $idioma = "";
    private $fc_libro;
    
    public function __get($attr){
        return $this->$attr;
    }

    public function __set($attr, $val){
        return $this->$val;
    }
}
<?php
require __DIR__.'/../../config.php';

class Prestamos{
    private $pdo;
    public function connect(){
        $connect="mysql:host=".DB_HOST.";dbname=".DB_NAME;
        $pdo=new PDO($connect,DB_USER,DB_PASSWORD);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo=$pdo;
    }
    public function getall(){
        $sql="SELECT * from prestamos";
        $this->connect();
        $stmt=$this->pdo->query($sql);
        $this->pdo=null;
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function findbyid(int $id){
        $sql="SELECT id_prestamo, id_usuariof, id_librof, fecha_pre, fecha_dev from prestamos where id_prestamo = $id";
        $this->connect();
        $stmt = $this->pdo->query($sql);
        $this->pdo=null;
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function add(Prestamo $prestamo){
        $sql = "INSERT INTO prestamos Values (default, :id_usuariof, :id_librof, :fecha_pre, :fecha_dev)";
        $this->connect();
        $stmt = $this->pdo->prepare($sql);
        $res = $stmt->execute(array(
            ':id_usuariof' => $prestamo->__get('id_usuariof'),
            ':id_librof' => $prestamo->__get('id_librof'),
            ':fecha_pre' => $prestamo->__get('fecha_pre'),
            ':fecha_dev' => $prestamo->__get('fecha_dev'),
        ));
    }
    public function update(Prestamo $prestamo){
        $sql = "UPDATE prestamos SET id_usuariof = :id_usuariof, id_librof = :id_librof, fecha_pre = :fecha_pre, fecha_dev = :fecha_dev WHERE id_prestamo = :id_prestamo";
        $this->connect();
        $stmt = $this->pdo->prepare($sql);
        $res = $stmt->execute(array(
            ':id_prestamo' => $prestamo->__get('id_prestamo'),
            ':id_usuariof' => $prestamo->__get('id_usuariof'),
            ':id_librof' => $prestamo->__get('id_librof'),
            ':fecha_pre' => $prestamo->__get('fecha_pre'),
            ':fecha_dev' => $prestamo->__get('fecha_dev'),
        ));
    }
    public function delete($id){
        $sql="DELETE FROM prestamos WHERE id_prestamo = :id";
        $this->connect();
        $stmt=$this->pdo->prepare($sql);
        $res=$stmt->execute(array(":id" => $id));
    }
}
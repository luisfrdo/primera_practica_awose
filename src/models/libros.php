<?php
require __DIR__.'/../../config.php';

class Libros{
    private $pdo;
    public function connect(){
        $connect="mysql:host=".DB_HOST.";dbname=".DB_NAME;
        $pdo=new PDO($connect,DB_USER,DB_PASSWORD);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo=$pdo;
    }
    public function getall(){
        $sql="SELECT * from libros";
        $this->connect();
        $stmt=$this->pdo->query($sql);
        $this->pdo=null;
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function findbyid(int $id){
        $sql="SELECT id_libro, titulo, autor, ano_pub, paginas, idioma, fc_libro from libros where id_libro = $id";
        $this->connect();
        $stmt = $this->pdo->query($sql);
        $this->pdo=null;
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function add(Libro $libro){
        $sql = "INSERT INTO libros Values (default, :titulo, :autor, :ano_pub, :paginas, :idioma, :fc_libro)";
        $this->connect();
        $stmt = $this->pdo->prepare($sql);
        $res = $stmt->execute(array(
            ':titulo' => $libro->__get('titulo'),
            ':autor' => $libro->__get('autor'),
            ':ano_pub' => $libro->__get('ano_pub'),
            ':paginas' => $libro->__get('paginas'),
            ':idioma' => $libro->__get('idioma'),
            ':fc_libro' => $libro->__get('fc_libro'),
        ));
    }
    public function update(Libro $libro){
        $sql = "UPDATE libros SET titulo = :titulo, autor = :autor, ano_pub = :ano_pub, paginas = :paginas, idioma = :idioma, fc_libro = :fc_libro WHERE id_libro = :id_libro";
        $this->connect();
        $stmt = $this->pdo->prepare($sql);
        $res = $stmt->execute(array(
            ':id_libro' => $libro->__get('id_libro'),
            ':titulo' => $libro->__get('titulo'),
            ':autor' => $libro->__get('autor'),
            ':ano_pub' => $libro->__get('ano_pub'),
            ':paginas' => $libro->__get('paginas'),
            ':idioma' => $libro->__get('idioma'),
            ':fc_libro' => $libro->__get('fc_libro'),
        ));
    }
    public function delete($id){
        $sql="DELETE FROM libros WHERE id_libro = :id";
        $this->connect();
        $stmt=$this->pdo->prepare($sql);
        $res=$stmt->execute(array(":id" => $id));
    }

}
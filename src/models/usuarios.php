<?php
require __DIR__.'/../../config.php';

class Usuarios{
    private $pdo;
    public function connect(){
        $connect="mysql:host=".DB_HOST.";dbname=".DB_NAME;
        $pdo=new PDO($connect,DB_USER,DB_PASSWORD);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo=$pdo;
    }
    public function getall(){
        $sql="SELECT * from usuarios";
        $this->connect();
        $stmt=$this->pdo->query($sql);
        $this->pdo=null;
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function findbyid(int $id){
        $sql="SELECT id_usuario, nombre, clave, correo, nivel, celular, fc_usuario from usuarios where id_usuario = $id";
        $this->connect();
        $stmt = $this->pdo->query($sql);
        $this->pdo=null;
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    public function add(Usuario $usuario){
        $sql = "INSERT INTO usuarios Values (default, :nombre, :clave, :correo, :nivel, :celular, :fc_usuario)";
        $this->connect();
        $stmt = $this->pdo->prepare($sql);
        $res = $stmt->execute(array(
            ':nombre' => $usuario->__get('nombre'),
            ':clave' => $usuario->__get('clave'),
            ':correo' => $usuario->__get('correo'),
            ':nivel' => $usuario->__get('nivel'),
            ':celular' => $usuario->__get('celular'),
            ':fc_usuario' => $usuario->__get('fc_usuario'),
        ));
    }
    public function update(Usuario $usuario){
        $sql = "UPDATE usuarios SET nombre = :nombre, clave = :clave, correo = :correo, nivel = :nivel, celular = :celular, fc_usuario = :fc_usuario WHERE id_usuario = :id_usuario";
        $this->connect();
        $stmt = $this->pdo->prepare($sql);
        $res = $stmt->execute(array(
            ':id_usuario' => $usuario->__get('id_usuario'),
            ':nombre' => $usuario->__get('nombre'),
            ':clave' => $usuario->__get('clave'),
            ':correo' => $usuario->__get('correo'),
            ':nivel' => $usuario->__get('nivel'),
            ':celular' => $usuario->__get('celular'),
            ':fc_usuario' => $usuario->__get('fc_usuario'),
        ));
    }
    public function delete($id){
        $sql="DELETE FROM usuarios WHERE id_usuario = :id";
        $this->connect();
        $stmt=$this->pdo->prepare($sql);
        $res=$stmt->execute(array(":id" => $id));
    }
}
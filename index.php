<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'src/models/libros.php';
require 'src/libro.php';

require 'src/models/usuarios.php';
require 'src/usuario.php';

require 'src/models/prestamos.php';
require 'src/prestamo.php';

$app = new \Slim\App;
$app->get('/libros', function(Request $request, Response $response){
    try{
        $librosBD = new Libros();
        $libros = $librosBD->getall();
        $response->withStatus(200);
        $response->withHeader('Content-Type', 'application/json');
        return $response->withJson($libros);
    }
    catch (PDOException $e){
        $response->withStatus(500);
        $response->withHeader('Content-Type', 'application/json');
        $error['err']=$e->getMessage();
        return $response->withJson($error);
    }
});
$app->get('/libros/{id}', function(Request $request, Response $response){
    try{
        $id=$request->getAttribute('id');
        $librosBD = new Libros();
        $libros = $librosBD->findbyid($id);
        $response->withStatus(200);
        $response->withHeader('Content-Type', 'application/json');
        return $response->withJson($libros);
    }
    catch (PDOException $e){
        $response->withStatus(500);
        $response->withHeader('Content-Type', 'application/json');
        $error['err']=$e->getMessage();
        return $response->withJson($error);
    }
});
$app->delete('/libros/{id}', function(Request $request, Response $response){
    try{
        $id=$request->getAttribute('id');
        $librosBD = new Libros();
        $libros = $librosBD->delete($id);
        $response->withStatus(200);
        $response->withHeader('Content-Type', 'application/json');
        $mensaje['ok']="Libro eliminado correctamente";
        return $response->withJson($mensaje);
    }
    catch (PDOException $e){
        $response->withStatus(500);
        $response->withHeader('Content-Type', 'application/json');
        $error['err']=$e->getMessage();
        return $response->withJson($error);
    }
});
$app->post('/libros', function (Request $request, Response $response) { 
    try {
      $libro = new Libro();    
      $libro->__set('titulo', $request->getParam('titulo'));
      $libro->__set('autor', $request->getParam('autor'));
      $libro->__set('ano_pub', $request->getParam('ano_pub'));
      $libro->__set('paginas', $request->getParam('paginas'));
      $libro->__set('idioma', $request->getParam('idioma'));
      $libro->__set('fc_libro', $request->getParam('fc_libro'));
  
      // adding libro in db
      $libros = new Libros();
      $libros->add($libro);
  
      // custom json response
      $response->withStatus(200);
      $response->withHeader('Content-Type', 'application/json');
      $message['ok'] = "Libro anadido correctamente";
      return $response->withJson($message);
  
    } catch (PDOException $e) {
      $response->withStatus(500);
      $response->withHeader('Content-Type', 'application/json');
      $error['err'] = $e->getMessage(); 
      return $response->withJson($error);
    }
  });
  $app->put('/libros', function (Request $request, Response $response) { 
    try {
  
      $libro = new Libro();
      $libro->__set('id_libro', $request->getParam('id_libro'));
      $libro->__set('titulo', $request->getParam('titulo'));
      $libro->__set('autor', $request->getParam('autor'));
      $libro->__set('ano_pub', $request->getParam('ano_pub'));
      $libro->__set('paginas', $request->getParam('paginas'));
      $libro->__set('idioma', $request->getParam('idioma'));
      $libro->__set('fc_libro', $request->getParam('fc_libro'));
  
  
      // updating libro in db
      $libros = new Libros();
      $libros->update($libro);
  
      // custom json response
      $response->withStatus(200);
      $response->withHeader('Content-Type', 'application/json');
      $message['ok'] = "libro actualizado correctamente";
      return $response->withJson($message);
  
    } catch (PDOException $e) {
      $response->withStatus(500);
      $response->withHeader('Content-Type', 'application/json');
      $error['err'] = $e->getMessage(); 
      return $response->withJson($error);
    }
  });

  //Usuarios
  $app->get('/usuarios', function(Request $request, Response $response){
    try{
        $usuariosBD = new Usuarios();
        $usuarios = $usuariosBD->getall();
        $response->withStatus(200);
        $response->withHeader('Content-Type', 'application/json');
        return $response->withJson($usuarios);
    }
    catch (PDOException $e){
        $response->withStatus(500);
        $response->withHeader('Content-Type', 'application/json');
        $error['err']=$e->getMessage();
        return $response->withJson($error);
    }
});
$app->get('/usuarios/{id}', function(Request $request, Response $response){
    try{
        $id=$request->getAttribute('id');
        $usuariosBD = new Usuarios();
        $usuarios = $usuariosBD->findbyid($id);
        $response->withStatus(200);
        $response->withHeader('Content-Type', 'application/json');
        return $response->withJson($usuarios);
    }
    catch (PDOException $e){
        $response->withStatus(500);
        $response->withHeader('Content-Type', 'application/json');
        $error['err']=$e->getMessage();
        return $response->withJson($error);
    }
});
$app->delete('/usuarios/{id}', function(Request $request, Response $response){
    try{
        $id=$request->getAttribute('id');
        $usuariosBD = new Usuarios();
        $usuarios = $usuariosBD->delete($id);
        $response->withStatus(200);
        $response->withHeader('Content-Type', 'application/json');
        $mensaje['ok']="Usuario eliminado correctamente";
        return $response->withJson($mensaje);
    }
    catch (PDOException $e){
        $response->withStatus(500);
        $response->withHeader('Content-Type', 'application/json');
        $error['err']=$e->getMessage();
        return $response->withJson($error);
    }
});
$app->post('/usuarios', function (Request $request, Response $response) { 
    try {
      $usuario = new Usuario();    
      $usuario->__set('nombre', $request->getParam('nombre'));
      $usuario->__set('clave', $request->getParam('clave'));
      $usuario->__set('correo', $request->getParam('correo'));
      $usuario->__set('nivel', $request->getParam('nivel'));
      $usuario->__set('celular', $request->getParam('celular'));
      $usuario->__set('fc_usuario', $request->getParam('fc_usuario'));
  
      // adding usuario in db
      $usuarios = new Usuarios();
      $usuarios->add($usuario);
  
      // custom json response
      $response->withStatus(200);
      $response->withHeader('Content-Type', 'application/json');
      $message['ok'] = "Usuario anadido correctamente";
      return $response->withJson($message);
  
    } catch (PDOException $e) {
      $response->withStatus(500);
      $response->withHeader('Content-Type', 'application/json');
      $error['err'] = $e->getMessage(); 
      return $response->withJson($error);
    }
  });
  $app->put('/usuarios', function (Request $request, Response $response) { 
    try {
  
      $usuario = new Usuario();
      $usuario->__set('id_usuario', $request->getParam('id_usuario'));
      $usuario->__set('nombre', $request->getParam('nombre'));
      $usuario->__set('clave', $request->getParam('clave'));
      $usuario->__set('correo', $request->getParam('correo'));
      $usuario->__set('nivel', $request->getParam('nivel'));
      $usuario->__set('celular', $request->getParam('celular'));
      $usuario->__set('fc_usuario', $request->getParam('fc_usuario'));
  
  
      // updating usuario in db
      $usuarios = new Usuarios();
      $usuarios->update($usuario);
  
      // custom json response
      $response->withStatus(200);
      $response->withHeader('Content-Type', 'application/json');
      $message['ok'] = "Usuario actualizado correctamente";
      return $response->withJson($message);
  
    } catch (PDOException $e) {
      $response->withStatus(500);
      $response->withHeader('Content-Type', 'application/json');
      $error['err'] = $e->getMessage(); 
      return $response->withJson($error);
    }
  });

  //Prestamos
  $app->get('/prestamos', function(Request $request, Response $response){
    try{
        $prestamosBD = new Prestamos();
        $prestamos = $prestamosBD->getall();
        $response->withStatus(200);
        $response->withHeader('Content-Type', 'application/json');
        return $response->withJson($prestamos);
    }
    catch (PDOException $e){
        $response->withStatus(500);
        $response->withHeader('Content-Type', 'application/json');
        $error['err']=$e->getMessage();
        return $response->withJson($error);
    }
});
$app->get('/prestamos/{id}', function(Request $request, Response $response){
    try{
        $id=$request->getAttribute('id');
        $prestamosBD = new Prestamos();
        $prestamos = $prestamosBD->findbyid($id);
        $response->withStatus(200);
        $response->withHeader('Content-Type', 'application/json');
        return $response->withJson($prestamos);
    }
    catch (PDOException $e){
        $response->withStatus(500);
        $response->withHeader('Content-Type', 'application/json');
        $error['err']=$e->getMessage();
        return $response->withJson($error);
    }
});
$app->delete('/prestamos/{id}', function(Request $request, Response $response){
    try{
        $id=$request->getAttribute('id');
        $prestamosBD = new Prestamos();
        $prestamos = $prestamosBD->delete($id);
        $response->withStatus(200);
        $response->withHeader('Content-Type', 'application/json');
        $mensaje['ok']="Prestamo eliminado correctamente";
        return $response->withJson($mensaje);
    }
    catch (PDOException $e){
        $response->withStatus(500);
        $response->withHeader('Content-Type', 'application/json');
        $error['err']=$e->getMessage();
        return $response->withJson($error);
    }
});
$app->post('/prestamos', function (Request $request, Response $response) { 
    try {
      $prestamo = new Prestamo();    
      $prestamo->__set('id_usuariof', $request->getParam('id_usuariof'));
      $prestamo->__set('id_librof', $request->getParam('id_librof'));
      $prestamo->__set('fecha_pre', $request->getParam('fecha_pre'));
      $prestamo->__set('fecha_dev', $request->getParam('fecha_dev'));
  
      // adding prestamo in db
      $prestamos = new Prestamos();
      $prestamos->add($prestamo);
  
      // custom json response
      $response->withStatus(200);
      $response->withHeader('Content-Type', 'application/json');
      $message['ok'] = "Prestamo anadido correctamente";
      return $response->withJson($message);
  
    } catch (PDOException $e) {
      $response->withStatus(500);
      $response->withHeader('Content-Type', 'application/json');
      $error['err'] = $e->getMessage(); 
      return $response->withJson($error);
    }
  });
  $app->put('/prestamos', function (Request $request, Response $response) { 
    try {
  
      $prestamo = new Prestamo();
      $prestamo->__set('id_prestamo', $request->getParam('id_prestamo'));
      $prestamo->__set('id_usuariof', $request->getParam('id_usuariof'));
      $prestamo->__set('id_librof', $request->getParam('id_librof'));
      $prestamo->__set('fecha_pre', $request->getParam('fecha_pre'));
      $prestamo->__set('fecha_dev', $request->getParam('fecha_dev'));
  
      // updating prestamo in db
      $prestamos = new Prestamos();
      $prestamos->update($prestamo);
  
      // custom json response
      $response->withStatus(200);
      $response->withHeader('Content-Type', 'application/json');
      $message['ok'] = "Prestamo actualizado correctamente";
      return $response->withJson($message);
  
    } catch (PDOException $e) {
      $response->withStatus(500);
      $response->withHeader('Content-Type', 'application/json');
      $error['err'] = $e->getMessage(); 
      return $response->withJson($error);
    }
  });
$app->run();
<?php
class Usuario {
    private $id_usuario;
    private $nombre = "";
    private $clave = "";
    private $correo = "";
    private $nivel;
    private $celular = "";
    private $fc_usuario;
    
    public function __get($attr){
        return $this->$attr;
    }

    public function __set($attr, $val){
        return $this->$attr = $val;
    }
}